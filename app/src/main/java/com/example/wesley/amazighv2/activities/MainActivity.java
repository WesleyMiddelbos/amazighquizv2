package com.example.wesley.amazighv2.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.wesley.amazighv2.R;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnpractice = findViewById(R.id.btnPractice);
        Button btnquiz = findViewById(R.id.btnQuiz);
        Button btnabout = findViewById(R.id.btnAbout);

        btnpractice.setOnClickListener(this);
        btnquiz.setOnClickListener(this);
        btnabout.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnPractice:
                Intent practiceIntent = new Intent(getApplicationContext(), CategoryActivity.class);
                practiceIntent.putExtra("type","practice");
                startActivity(practiceIntent);
                break;
            case R.id.btnQuiz:
                Intent quizIntent = new Intent(getApplicationContext(),CategoryActivity.class);
                quizIntent.putExtra("type","quiz");
                startActivity(quizIntent);
                break;
            case R.id.btnAbout:
                Intent aboutIntent = new Intent(getApplicationContext(), AboutUsActivity.class);
                startActivity(aboutIntent);
                break;
        }
    }
}
