package com.example.wesley.amazighv2.fragments;

import android.content.res.Resources;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.wesley.amazighv2.Models.Theme;
import com.example.wesley.amazighv2.Models.Word;
import com.example.wesley.amazighv2.R;

import java.util.ArrayList;

public class PracticeFragment extends Fragment implements View.OnClickListener, MediaPlayer.OnCompletionListener {

    public ArrayList<Word> words;

    private TextView translateTv;
    private ImageView imageView;
    private TextView wordTv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_practice, container, false);
        this.imageView = rootView.findViewById(R.id.imageViewQuiz);
        this.wordTv = rootView.findViewById(R.id.wordTvPractice);
        this.translateTv = rootView.findViewById(R.id.translateTvPractice);
        Word tranlsate = words.get(0);
        Word amazigh = words.get(1);

        this.wordTv.setText(amazigh.getWord());
        this.translateTv.setText(tranlsate.getWord());

        Resources resources = this.getResources();
        int resourceId = resources.getIdentifier(amazigh.getImage().getName(),"drawable", getContext().getPackageName());
        if(resourceId == 0){
            this.imageView.setImageResource(R.drawable.splashscreen);
        }else {
            this.imageView.setImageResource(resourceId);
        }
        this.imageView.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        this.playSound();
    }

    private void playSound(){
        Word amazigh = words.get(1);
        Resources resources = this.getResources();
        int resourceId = resources.getIdentifier(amazigh.getSound().getName(), "raw", getContext().getPackageName());
        if(resourceId != 0){
            MediaPlayer sound = MediaPlayer.create(getContext(), resourceId);
            sound.setAudioStreamType(AudioManager.STREAM_MUSIC);
            sound.start();
            sound.setOnCompletionListener(this);
        }
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        mediaPlayer.stop();
        mediaPlayer.reset();
        mediaPlayer.release();
    }
}
