package com.example.wesley.amazighv2.Models;

public class Word {

    private String word;
    private String translations;
    private Sound sound;
    private Image image;

    public void setWord(String word){this.word = word;}

    public void setTranslation(String translations){this.translations = translations;}

    public void setSound(Sound sound){this.sound = sound;}

    public void setImage(Image image){this.image = image;}

    public String getWord(){return this.word;}

    public String getTranslation(){return this.translations;}

    public Sound getSound(){return this.sound;}

    public Image getImage(){return this.image;}

}
