package com.example.wesley.amazighv2.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.example.wesley.amazighv2.Models.Theme;
import com.example.wesley.amazighv2.fragments.PracticeFragment;

public class PracticeFragmentAdapter extends FragmentStatePagerAdapter {

    public static Theme theme;

    public PracticeFragmentAdapter(FragmentManager fm){
        super(fm);
    }
    @Override
    public Fragment getItem(int i) {
        PracticeFragment fragment = new PracticeFragment();
        fragment.words = theme.getWords().get(i);
        return fragment;
    }

    @Override
    public int getCount() {
        return theme.getWords().size();
    }
}
