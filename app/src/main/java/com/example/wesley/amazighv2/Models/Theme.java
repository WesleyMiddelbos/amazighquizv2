package com.example.wesley.amazighv2.Models;

import java.util.ArrayList;

public class Theme {

    private String name;
    private ArrayList<ArrayList<Word>> words;

    public void setName(String name){this.name = name;}

    public void setWords(ArrayList<ArrayList<Word>> words){this.words = words;}

    public String getName(){return this.name;}

    public ArrayList<ArrayList<Word>> getWords(){return this.words;}

}
