package com.example.wesley.amazighv2.Stores;

import com.example.wesley.amazighv2.Models.Image;
import com.example.wesley.amazighv2.Models.Sound;
import com.example.wesley.amazighv2.Models.Theme;
import com.example.wesley.amazighv2.Models.Word;
import com.example.wesley.amazighv2.parsers.jsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

public class ThemeStore {

    public static ArrayList<Theme> themes(){

        JSONObject object = jsonParser.content;
        ArrayList<Theme> themes = new ArrayList<Theme>();
        Iterator<String> keys = object.keys();
        while(keys.hasNext()){
            Theme theme = new Theme();
            String key = keys.next();
            ArrayList<ArrayList<Word>> words = new ArrayList<ArrayList<Word>>();
            theme.setName(key);
            try {
                JSONArray wordsArray = object.getJSONArray(key);
                for(int i=0;i<wordsArray.length();i++){
                    ArrayList<Word> wordCollection = new ArrayList<Word>();
                    JSONObject wordObject = wordsArray.getJSONObject(i);
                    Iterator<String> wordKeys = wordObject.keys();
                    while(wordKeys.hasNext()){
                        String wordKey = wordKeys.next();
                        JSONObject dataObject = wordObject.getJSONObject("data");
                        Sound sound = new Sound();
                        sound.setName(dataObject.getString("sound"));
                        Image image = new Image();
                        image.setName(dataObject.getString("photo"));
                        if(!wordKey.equals("data")){
                            Word word = new Word();
                            word.setTranslation(wordKey);
                            JSONObject translationObject = wordObject.getJSONObject(wordKey);
                            word.setWord(translationObject.getString("word"));
                            word.setImage(image);
                            word.setSound(sound);
                            wordCollection.add(word);
                        }
                    }
                    words.add(wordCollection);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            theme.setWords(words);
            themes.add(theme);
        }
        return themes;

    }

    public static Theme theme(String themeName){

        JSONObject object = jsonParser.content;
        Iterator<String> keys = object.keys();
        while(keys.hasNext()){
            Theme theme = new Theme();
            ArrayList<ArrayList<Word>> words = new ArrayList<ArrayList<Word>>();
            String key = keys.next();
            if(key.equals(themeName)) {
                try {
                    JSONArray wordsArray = object.getJSONArray(key);
                    for (int i = 0; i < wordsArray.length(); i++) {
                        ArrayList<Word> wordCollection = new ArrayList<Word>();
                        JSONObject wordObject = wordsArray.getJSONObject(i);
                        Iterator<String> wordKeys = wordObject.keys();
                        while (wordKeys.hasNext()) {
                            String wordKey = wordKeys.next();
                            JSONObject dataObject = wordObject.getJSONObject("data");
                            Sound sound = new Sound();
                            sound.setName(dataObject.getString("sound"));
                            Image image = new Image();
                            image.setName(dataObject.getString("photo"));
                            if (!wordKey.equals("data")) {
                                Word word = new Word();
                                word.setTranslation(wordKey);
                                JSONObject translationObject = wordObject.getJSONObject(wordKey);
                                word.setWord(translationObject.getString("word"));
                                word.setImage(image);
                                word.setSound(sound);
                                wordCollection.add(word);
                            }
                        }
                        words.add(wordCollection);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                theme.setWords(words);
                return theme;
            }
        }
        return null;
    }

    public static ArrayList<Word> getAnswerWords(Theme theme, int currentWord){
        ArrayList<Word> answers = new ArrayList<Word>();
        answers.add(theme.getWords().get(currentWord).get(0));
        for(int i=0;i<5;i++){
            int randomWord = getCorrectRandom(theme, currentWord, answers);
            answers.add(theme.getWords().get(randomWord).get(0));
        }
        Collections.shuffle(answers);
        return answers;
    }

    private static int getCorrectRandom(Theme theme, int awnser, ArrayList<Word> list){
        Random random = new Random();
        int randomWord = random.nextInt(theme.getWords().size());
        if(randomWord != awnser && !list.contains(theme.getWords().get(randomWord).get(0))){
            return randomWord;
        }else{
            return getCorrectRandom(theme,awnser,list);
        }
    }
}
