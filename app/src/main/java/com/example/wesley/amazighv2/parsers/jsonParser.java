package com.example.wesley.amazighv2.parsers;

import android.content.Context;
import com.example.wesley.amazighv2.R;
import org.json.JSONObject;
import java.io.InputStream;

public class jsonParser {

    public static JSONObject content;

    public static JSONObject parse(Context context){
        try {
            InputStream is = context.getResources().openRawResource(R.raw.content);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String json = new String(buffer, "UTF-8");
            content = new JSONObject(json);
            return content;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

}
