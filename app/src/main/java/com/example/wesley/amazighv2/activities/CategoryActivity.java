package com.example.wesley.amazighv2.activities;


import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toolbar;

import com.example.wesley.amazighv2.Models.Theme;
import com.example.wesley.amazighv2.R;
import com.example.wesley.amazighv2.Stores.ThemeStore;

import java.util.ArrayList;

public class CategoryActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout layout;
    public static ArrayList<Theme> themes;
    private static Theme selectedTheme;
    private String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        this.type = getIntent().getExtras().getString("type");

        themes = ThemeStore.themes();
        this.layout = findViewById(R.id.scrollViewContainerMain);

        this.generateUi();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.backmenu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.goback) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void generateUi(){

        for(int i=0;i<themes.size();i++){
            Theme theme = themes.get(i);
            Button button = new Button(this);
            button.setText(theme.getName());
            button.setTag(R.string.theme,theme);
            button.setOnClickListener(this);
            this.layout.addView(button);
        }

    }

    @Override
    public void onClick(View v) {
        Button button = (Button)v;
        selectedTheme = (Theme)button.getTag(R.string.theme);
        if(type.equals("quiz")){
            Intent intent = new Intent(this,QuizActivity.class);
            intent.putExtra("theme",selectedTheme.getName());
            startActivity(intent);
        }else if(this.type.equals("practice")){
            Intent intent = new Intent(this,PracticeActivity.class);
            intent.putExtra("theme",selectedTheme.getName());
            startActivity(intent);
        }
    }
}
