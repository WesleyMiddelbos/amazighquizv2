package com.example.wesley.amazighv2.activities;

import android.content.Intent;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.wesley.amazighv2.Models.Image;
import com.example.wesley.amazighv2.Models.Theme;
import com.example.wesley.amazighv2.Models.Word;
import com.example.wesley.amazighv2.R;
import com.example.wesley.amazighv2.Stores.ThemeStore;

import java.util.ArrayList;

public class QuizActivity extends AppCompatActivity implements View.OnClickListener {


    private static Theme theme;
    private int wordKey = 0;
    private String currrentWord;
    private ArrayList<Word> awnsers;

    private TextView word;
    private ImageView awnser1;
    private ImageView awnser2;
    private ImageView awnser3;
    private ImageView awnser4;
    private ImageView awnser5;
    private ImageView awnser6;
    private static int good;
    private static int bad;
    private static int incorrect;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        this.word = findViewById(R.id.textViewQuiz);
        this.awnser1 = findViewById(R.id.answer1);
        this.awnser2 = findViewById(R.id.answer2);
        this.awnser3 = findViewById(R.id.answer3);
        this.awnser4 = findViewById(R.id.answer4);
        this.awnser5 = findViewById(R.id.answer5);
        this.awnser6 = findViewById(R.id.answer6);

        this.word.setOnClickListener(this);
        this.awnser1.setOnClickListener(this);
        this.awnser2.setOnClickListener(this);
        this.awnser3.setOnClickListener(this);
        this.awnser4.setOnClickListener(this);
        this.awnser5.setOnClickListener(this);
        this.awnser6.setOnClickListener(this);

        String themeName = getIntent().getStringExtra("theme");
        theme = ThemeStore.theme(themeName);

        this.currrentWord = theme.getWords().get(0).get(0).getWord();

        awnsers = ThemeStore.getAnswerWords(theme, wordKey);
        this.setAwnsers(awnsers);
        this.setTextViewWord();
        this.playSound();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.backmenu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.goback) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void generateAnswers(){
        this.currrentWord = theme.getWords().get(wordKey).get(0).getWord();
        awnsers = ThemeStore.getAnswerWords(theme, wordKey);
        this.setAwnsers(awnsers);
    }

    private void setTextViewWord(){
        Word word = theme.getWords().get(wordKey).get(1);
        this.word.setText(word.getWord());
    }

    private void playSound(){
        ArrayList<Word> words = theme.getWords().get(this.wordKey);
        Word amazigh = words.get(1);
        Resources resources = this.getResources();
        int resourceId = resources.getIdentifier(amazigh.getSound().getName(), "raw", this.getPackageName());
        if(resourceId == 0){
            Toast.makeText(this,"Geluid wordt later toegevoegd",Toast.LENGTH_LONG).show();
        }else {
            MediaPlayer sound = MediaPlayer.create(this, resourceId);
            sound.setAudioStreamType(AudioManager.STREAM_MUSIC);
            sound.start();
        }
    }

    private void setAwnsers(ArrayList<Word> words){
        ArrayList<ImageView> images = new ArrayList<ImageView>();
        images.add(awnser1);
        images.add(awnser2);
        images.add(awnser3);
        images.add(awnser4);
        images.add(awnser5);
        images.add(awnser6);
        Resources resources = this.getResources();
        for(int i=0;i<words.size();i++){
            Word word = words.get(i);
            ImageView imageView = images.get(i);
            imageView.setTag(R.string.correct,false);
            if(word.getWord().equals(this.currrentWord)){
                imageView.setTag(R.string.correct,true);
            }
            Image image = word.getImage();
            int resourceId = resources.getIdentifier(image.getName(),"drawable", this.getPackageName());
            if(resourceId == 0){
                imageView.setImageResource(R.drawable.splashscreen);
            }else {
                imageView.setImageResource(resourceId);
            }
        }
    }

    private void nextWordOrExit(){
        if (theme.getWords().size() - 1 > wordKey) {
            wordKey++;
            this.setTextViewWord();
            this.generateAnswers();
            this.playSound();
        } else {
            int amountOfWords = theme.getWords().size();
            float score = (float)10/amountOfWords*good;
            Toast.makeText(this, "Quiz is voorbij. Je score is een "+Math.round(score), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.textViewQuiz) {
            this.playSound();
        } else {
            if ((boolean) v.getTag(R.string.correct)) {
                good++;
                bad = 0;
            }else{
                bad++;
                if(bad > 3){
                    bad = 0;
                    incorrect++;
                    Toast.makeText(this, "Te veel foute antwoorden ingevoerd", Toast.LENGTH_SHORT).show();
                    this.nextWordOrExit();
                    return;
                }else{
                    return;
                }
            }
            this.nextWordOrExit();
        }
    }
}
