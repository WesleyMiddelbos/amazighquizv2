package com.example.wesley.amazighv2.activities;

import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.wesley.amazighv2.Models.Theme;
import com.example.wesley.amazighv2.R;
import com.example.wesley.amazighv2.Stores.ThemeStore;
import com.example.wesley.amazighv2.adapters.PracticeFragmentAdapter;


public class PracticeActivity extends AppCompatActivity {

    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;

    private static Theme theme;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practice);

        String themeName = getIntent().getStringExtra("theme");
        theme = ThemeStore.theme(themeName);

        mPager = findViewById(R.id.slider);
        mPagerAdapter = new PracticeFragmentAdapter(getSupportFragmentManager());
        PracticeFragmentAdapter.theme = theme;
        mPager.setAdapter(mPagerAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.backmenu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.goback) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
